import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { ToastController, NavController } from '@ionic/angular';
import swal from 'sweetalert';
import { EnvService } from './env.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})

export class WebService {

    errorWording = "Silakan coba lagi";

    token: any;
    constructor(
        private http: HttpClient,
        private storage: Storage,
        private navCtrl: NavController,
        private router: Router,
        private env: EnvService,
        private toastController: ToastController
    ) { }

    // home
    getTodayTargets() {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const httpOpt = {
                headers: new HttpHeaders({
                    'content-type': 'application/json',
                    authorization: 'Bearer ' + token
                })
            };
            this.http.post(this.env.API_URL + 'get-today-target', {} , httpOpt).subscribe(
                (resData: any) => {
                    if (resData.result) {
                        resolve(resData.result);
                    }
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    // check in
    storeCheckIn(data, image) {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const body = {
                absent: data.absent,
                lat: data.latitude,
                lng: data.longitude,
                image: image
            };
            const httpOpt = {
                headers: new HttpHeaders({
                    'content-type': 'application/json',
                    authorization: 'Bearer ' + token
                })
            };

            this.http.post(this.env.API_URL + 'store-checkin', JSON.stringify(body), httpOpt).subscribe(
                (resData: any) => {
                    resolve(resData.result);
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    storeMovement(data) {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const body = {
                lat: data.lat,
                lng: data.lng
            };

            const httpOpt = {
                headers: new HttpHeaders({
                    'content-type': 'application/json',
                    authorization: 'Bearer ' + token
                })
            };

            this.http.post(this.env.API_URL + 'store-movement', JSON.stringify(body), httpOpt).subscribe(
                (resData: any) => {
                    console.log(resData);
                    resolve(resData.result);
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    // planning
    getAttendance() {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const httpOpt = {
                headers: new HttpHeaders({
                    'content-type': 'application/json',
                    authorization: 'Bearer ' + token
                })
            };

            this.http.post(this.env.API_URL + 'get-attendance', {}, httpOpt).subscribe(
                (resData: any) => {
                    resolve(resData.result);
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    getPlanning() {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const httpOpt = {
                headers: new HttpHeaders({
                    'content-type': 'application/json',
                    authorization: 'Bearer ' + token
                })
            };

            this.http.post(this.env.API_URL + 'get-planning', {}, httpOpt).subscribe(
                (resData: any) => {
                    resolve(resData.result);
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    storePlanning(data, nextDay) {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const body = {
                lead_target: data.leadTarget,
                apply_target: data.applyTarget,
                not_apply: data.notApplyTarget,
                pending_data: data.visitPendingData,
                on_boarding: data.visitOnBoarding,
                tanggal: nextDay
            };
            const httpOpt = {
                headers: new HttpHeaders({
                    'content-type': 'application/json',
                    authorization: 'Bearer ' + token
                })
            };
            this.http.post(this.env.API_URL + 'store-planning', JSON.stringify(body), httpOpt).subscribe(
                (resData: any) => {
                    resolve(resData.result);
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    storeNotWork(data) {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const body = {
                status: data.status,
                reason: data.reason,
                image: data.image
            };
            const httpOpt = {
                headers: new HttpHeaders({
                'content-type': 'application/json',
                authorization: 'Bearer ' + token
                })
            };
            this.http.post(this.env.API_URL + 'store-notwork', JSON.stringify(body), httpOpt).subscribe(
                (resData: any) => {
                    resolve(resData.result);
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    getDataActual() {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const httpOpt = {
                headers: new HttpHeaders({
                    'content-type': 'application/json',
                    authorization: 'Bearer ' + token
                })
            };

            this.http.post(this.env.API_URL + 'get-data-actual', {}, httpOpt).subscribe(
                (resData: any) => {
                    resolve(resData.result);
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    getDataPlanning() {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const httpOpt = {
                headers: new HttpHeaders({
                    'content-type': 'application/json',
                    authorization: 'Bearer ' + token
                })
            };

            this.http.post(this.env.API_URL + 'get-data-planning', {}, httpOpt).subscribe(
                (resData: any) => {
                    resolve(resData.result);
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    getBorrowerStatuses() {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const httpOpt = {
                headers: new HttpHeaders({
                    'content-type': 'application/json',
                    authorization: 'Bearer ' + token
                })
            };

            this.http.post(this.env.API_URL + 'get-borrower-statuses', {}, httpOpt).subscribe(
                (resData: any) => {
                    resolve(resData.result);
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    // reporting
    storeNewLead(data) {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const body = {
                lead_name: data.lead_name,
                outlet_name: data.outlet_name,
                outlet_phone: data.outlet_phone,
                outlet_address: data.outlet_address,
                lat: data.latitude,
                lng: data.longitude
            };
            const httpOpt = {
                headers: new HttpHeaders({
                'content-type': 'application/json',
                authorization: 'Bearer ' + token
                })
            };
            this.http.post(this.env.API_URL + 'new-lead', JSON.stringify(body), httpOpt).subscribe(
                (resData: any) => {
                    resolve(resData.result);
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    getNotApplyLeads() {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const httpOpt = {
                headers: new HttpHeaders({
                    'content-type': 'application/json',
                    authorization: 'Bearer ' + token
                })
            };
            this.http.post(this.env.API_URL + 'get-not-apply-lead', {} , httpOpt).subscribe(
                (resData: any) => {
                    if (resData.result) {
                        resolve(resData.result);
                    }
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    storeNotApplyLead(data) {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const body = {
                lead_id: data.id_outlet,
                result: data.visit_result
            };
            const httpOpt = {
                headers: new HttpHeaders({
                'content-type': 'application/json',
                authorization: 'Bearer ' + token
                })
            };
            this.http.post(this.env.API_URL + 'not-apply-lead', JSON.stringify(body), httpOpt).subscribe(
                (resData: any) => {
                    resolve(resData.result);
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    getPendingDataLeads() {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const httpOpt = {
                headers: new HttpHeaders({
                    'content-type': 'application/json',
                    authorization: 'Bearer ' + token
                })
            };
            this.http.post(this.env.API_URL + 'get-pending-data-lead', {} , httpOpt).subscribe(
                (resData: any) => {
                    if (resData.result) {
                        resolve(resData.result);
                    }
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    getPendingOnBoardingLeads() {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const httpOpt = {
                headers: new HttpHeaders({
                    'content-type': 'application/json',
                    authorization: 'Bearer ' + token
                })
            };
            this.http.post(this.env.API_URL + 'get-pending-on-boarding-lead', {} , httpOpt).subscribe(
                (resData: any) => {
                    if (resData.result) {
                        resolve(resData.result);
                    }
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    storePendingDataApply(data, image) {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const body = {
                lead_id: data.id_outlet,
                result: data.visit_result,
                image: image
            };
            const httpOpt = {
                headers: new HttpHeaders({
                'content-type': 'application/json',
                authorization: 'Bearer ' + token
                })
            };
            this.http.post(this.env.API_URL + 'pending-data-lead', JSON.stringify(body), httpOpt).subscribe(
                (resData: any) => {
                    resolve(resData.result);
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    storePendingOnBoardingApply(data, image) {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const body = {
                lead_id: data.id_outlet,
                result: data.visit_result,
                status_result: data.status_result,
                image: image

            };
            const httpOpt = {
                headers: new HttpHeaders({
                'content-type': 'application/json',
                authorization: 'Bearer ' + token
                })
            };
            this.http.post(this.env.API_URL + 'pending-on-boarding-lead', JSON.stringify(body), httpOpt).subscribe(
                (resData: any) => {
                    resolve(resData.result);
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    // summary
    getDataSummary() {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const httpOpt = {
                headers: new HttpHeaders({
                    'content-type': 'application/json',
                    authorization: 'Bearer ' + token
                })
            };

            this.http.post(this.env.API_URL + 'get-summary', {}, httpOpt).subscribe(
                (resData: any) => {
                    resolve(resData.result);
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    getApplicationStatuses() {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const httpOpt = {
                headers: new HttpHeaders({
                    'content-type': 'application/json',
                    authorization: 'Bearer ' + token
                })
            };

            this.http.post(this.env.API_URL + 'get-application-statuses', {}, httpOpt).subscribe(
                (resData: any) => {
                    resolve(resData.result);
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }

    getDataSummaryByFilter(data) {
        return new Promise(async (resolve, reject) => {
            const token = await this.storage.get('token');
            const body = {
                date_start: data.date_start != null ? moment(new Date(data.date_start)).format('YYYY-MM-DD') : null,
                date_end: data.date_end != null ? moment(new Date(data.date_end)).format('YYYY-MM-DD') : moment(new Date(data.date_start)).format('YYYY-MM-DD'),
                lead_status: data.lead_status,
                apps_status: data.apps_status

            };
            const httpOpt = {
                headers: new HttpHeaders({
                    'content-type': 'application/json',
                    authorization: 'Bearer ' + token
                })
            };

            this.http.post(this.env.API_URL + 'get-summary-by-filter', JSON.stringify(body), httpOpt).subscribe(
                (resData: any) => {
                    resolve(resData.result);
                },
                error => {
                    swal('Error', this.errorWording, 'error');
                    reject(error);
                }
            );
        });
    }
}
