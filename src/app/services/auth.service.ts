import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { ToastController, NavController } from '@ionic/angular';
import swal from 'sweetalert';
import { EnvService } from './env.service';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  errorWording = "Silakan coba lagi";
  isLoggedIn = false;
  token: any;

  dataUserLocal: any;

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private navCtrl: NavController,
    private env: EnvService,
    private toastController: ToastController
  ) { }

  loginLocal(data) {
    console.log(data);
    let role = "employee";
    this.http.get("assets/data/auth.txt").subscribe((result: any) => {
      for (let i = 0; i < result.length; i++) {
        if (data.username === result[i].username && data.password === result[i].password) {
          this.storage.set('token', result[i].token);
          role = result[i].role;
          const session = {
            name: result[i].name,
            role: result[i].role
          };
          this.storage.set('session', session);
          break;
        }

      }

      if (role == 'employee') {
        console.log('asd')
        this.navCtrl.navigateRoot('tabs/home');
      } else {
        console.log('bcd')
        this.navCtrl.navigateRoot('tabs/ticket');
      }
    })
  }

  login(data) {
    return new Promise(async (resolve, reject) => {
      const body = {
        username: data.username,
        password: data.password,
      };
      const httpOpt = {
        headers: new HttpHeaders({
          'content-type': 'application/json'
        })
      };
      this.http.post(this.env.API_URL + 'login', JSON.stringify(body), httpOpt).subscribe(
        (resData: any) => {
          console.log(resData);
          if (resData.result.code === 200) {
              this.storage.set('token', resData.result.data.token);
              const session = {
                name: resData.result.data.name,
                area: resData.result.data.area
              };
              this.storage.set('session', session);
              this.navCtrl.navigateRoot('/');
          } else {
            swal('', resData.result.data, resData.result.message);
          }
        },
        error => {
          swal('Error', this.errorWording, 'error');

          reject(error);
        }
      );
    });
  }

  changePassword(data) {
    return new Promise(async (resolve, reject) => {
      const token = await this.storage.get('token');
      const body = {
        old_password: data.old_password,
        new_password: data.new_password
      };
      const httpOpt = {
        headers: new HttpHeaders({
          'content-type': 'application/json',
          authorization: 'Bearer ' + token
        })
      };
      this.http.post(this.env.API_URL + 'change-password', JSON.stringify(body), httpOpt).subscribe(
        (resData: any) => {
          resolve(resData.result);
        },
        error => {
          swal('Error', this.errorWording, 'error');
          reject(error);
        }
      );
    });
  }

  forgotPassword(data) {
    return new Promise(async (resolve, reject) => {
      const token = await this.storage.get('token');
      const body = {
        email: data.email,
      };
      const httpOpt = {
        headers: new HttpHeaders({
          'content-type': 'application/json',
          authorization: 'Bearer ' + token
        })
      };
      this.http.post(this.env.API_URL + 'forgot-password', JSON.stringify(body), httpOpt).subscribe(
        (resData: any) => {
          resolve(resData.result);
        },
        error => {
          swal('Error', this.errorWording, 'error');
          reject(error);
        }
      );
    });
  }

  getUser() {
    return new Promise(async (resolve, reject) => {
      const token = await this.storage.get('token');
      const httpOpt = {
        headers: new HttpHeaders({
          'content-tpye': 'application/json',
          authorization: 'Bearer ' + token
        })
      };

      this.http.post(this.env.API_URL + 'get-user', {}, httpOpt).subscribe(
        (resData: any) => {
          resolve(resData.result);
        },
        error => {
          swal('Error', this.errorWording, 'error');
          reject(error);
        }
      );
    });
  }

  logout() {
    const session = {
      name: '',
    };
    this.storage.set('session', session);
    this.storage.set('token', '');
    this.navCtrl.navigateRoot('/login');
  }
}
