import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../home/home.module').then(m => m.HomePageModule)
          }
        ]
      },
      {
        path: 'checkin',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../check-in/check-in.module').then(m => m.CheckInPageModule)
          }
        ]
      },
      {
        path: 'ticket',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../ticket/ticket.module').then(m => m.TicketPageModule)
          },
          {
            path: 'create',
            loadChildren: () =>
              import('../new-ticket/new-ticket.module').then(m => m.NewTicketPageModule)
          },
          {
            path: 'detail',
            loadChildren: () => import('../ticket-detail/ticket-detail.module').then(m => m.TicketDetailPageModule)
          }
        ]
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../profile/profile.module').then(m => m.ProfilePageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: 'tabs/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
