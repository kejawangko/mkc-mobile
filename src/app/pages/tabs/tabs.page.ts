import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  userType: any;

  constructor(private storage: Storage) {}

  ionViewWillEnter() {
    this.storage.get('session').then((value) => {
      if (value != null && value !== '') {
        this.userType = value.role;
      }
    });
  }
}
