import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.page.html',
  styleUrls: ['./ticket.page.scss'],
})
export class TicketPage implements OnInit {
  userType: any;

  constructor(private navCtrl: NavController,
              private storage: Storage,
              private auth: AuthService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.storage.get('session').then((value) => {
      if (value != null && value !== '') {
        this.userType = value.role;
      }
    });
  }

  viewDetail() {
    this.navCtrl.navigateForward(['tabs/ticket/ticket-detail']);
  }

  logout() {
    this.auth.logout();
  }

}
