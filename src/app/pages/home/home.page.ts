import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  constructor(private auth: AuthService,
              private storage: Storage,
              private navCtrl: NavController) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    
  }

  logout() {
    this.auth.logout();
  }

}
