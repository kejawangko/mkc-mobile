import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Loc } from 'src/app/models/location';
import { WebService } from 'src/app/services/web.service';
import { Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';

import swal from 'sweetalert';

@Component({
  selector: 'app-check-in',
  templateUrl: './check-in.page.html',
  styleUrls: ['./check-in.page.scss'],
})
export class CheckInPage implements OnInit {
  // initialize untuk maps
  marker: Loc;
  lat = -6.1881882;
  lng = 106.7925963;
  location: any;

  // initialize untuk upload foto
  imageUrl = '';
  base64Image: string;
  imageToUpload: string;

  // initialize untuk form
  checkInForm: FormGroup;

  // initialize loading
  isLoading: any;
  
  @ViewChild('map', { static: true }) mapElement: ElementRef;
  constructor(
    private geoloc: Geolocation,
    private webService: WebService,
    private router: Router,
    private camera: Camera,
    private actionSheetController: ActionSheetController,
    private globalService: GlobalService) {
      
    this.marker = new Loc(this.lat, this.lng);
  }

  private initializeForm() {
    this.checkInForm = new FormGroup({
      latitude: new FormControl(null),
      longitude: new FormControl(null),
      absent: new FormControl(false)
    });
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Select Image source',
      buttons: [{
        text: 'Take Photo',
        handler: () => {
          this.onTakePhoto();
        }
      }]
    });
    await actionSheet.present();
  }

  ngOnInit() {
    this.base64Image = '';
    this.initializeForm();
  }

  onSetMarker(event: any) {
    this.marker = new Loc(event.coords.lat, event.coords.lng);
    this.lat = this.marker.lat;
    this.lng = this.marker.lng;
  }

  onLocate() {
    this.globalService.presentLoading().then(resp => {
      this.geoloc.getCurrentPosition()
        .then(
          currLocation => {
            this.location = new Loc(currLocation.coords.latitude, currLocation.coords.longitude);
            this.marker = new Loc(currLocation.coords.latitude, currLocation.coords.longitude);
            this.lat = this.location.lat;
            this.lng = this.location.lng;
          }
        )
        .catch(
          error => {
            console.log(error);
          }
        );
      this.globalService.dismissLoading();
    });
  }

  onSubmit() {
    // this.globalService.presentLoading();
    console.log(this.checkInForm.value);
    // this.webService.getAttendance().then((result: any) => {
    //   if (result.code === 200) {
    //     if (result.data) {
    //       swal('', 'Anda sudah melakukan check in', 'info');
    //     } else {
    //       // tslint:disable-next-line: no-shadowed-variable
    //       this.webService.storeCheckIn(this.checkInForm.value, this.base64Image).then((result: any) => {
    //         swal('', result.data, result.message);
    //         if (result.code === 200) {
    //           if (this.checkInForm.value.absent === false) {
    //             console.log('masuk kerja');
                
    //           }
    //           this.router.navigateByUrl('/tabs/home-tab');
    //         }
    //       });
    //     }
    //   }
      // this.globalService.dismissLoading();
    // });
  }

  onTakePhoto() {
    const options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      correctOrientation: true
    };

    this.camera.getPicture(options).
      then((imageData) => {
        this.base64Image = 'data:image/jpeg;base64,' + imageData;
      },
      (err) => {
        this.base64Image = 'error';
      });
  }

}
