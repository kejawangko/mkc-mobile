import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    private navCtrl: NavController
  ) {
    this.initializeApp();
  }

  isLogin = 0;

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

    // ini buat live
    this.storage.get('token').then((value) => {
      if (value != null && value !== '') {
        this.isLogin = 1;
        this.storage.get('session').then((value) => {
          if (value != null && value !== '') {
            if (value.role == 'employee') {
              this.navCtrl.navigateRoot('tabs/home');
            } else {
              this.navCtrl.navigateRoot('tabs/ticket');
            }
          }
        });
      } else {
        this.navCtrl.navigateRoot('/login');
      }
    });
  }
}
